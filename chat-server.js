const socketIO = require('socket.io');
const { createServer } = require('http');

const server = createServer().listen(3000);
const io = socketIO(server);
var users = [];

io.on('connection', socket => {
    console.log(`${io.engine.clientsCount} connections`);

    socket.on("user_connected", (room, username) => {
        users[socket.id] = username;
        socket.join(room);
        socket.to(room).broadcast.emit('user-connect', username);
        console.log(`${username} and ${room}`);
        console.log(users);
    })
    socket.on('disconnect', ()=> {
        console.log(`${socket.id} just disconnected`);
    });
    


    socket.on('chat', data => {
        console.log(data);
        console.log(`${data.sender} has sent ${data.message} to ${data.roomName}`);
        socket.to(data.roomName).broadcast.emit("chat-message", data);
        // io.sockets.emit("chat-message", message, socket.id);
    });
});

console.log("socket server");


