import express from 'express';
import routes from "./src/routes/crmRoutes";
import mongoose from 'mongoose';
import parser from "body-parser";

const app = express();
const server = require('http').createServer(app);
const PORT = 4000;
const io = require('socket.io')(server);

//socket
var users = [];

io.on('connection', socket => {
    console.log(`${io.engine.clientsCount} connections`);

    socket.on("user_connected", (room, username) => {
        users[socket.id] = username;
        socket.join(room);
        socket.to(room).broadcast.emit('user-connect', username);
        console.log(`${username} and ${room}`);
        console.log(users);
    })
    socket.on('disconnect', ()=> {
        console.log(`${socket.id} just disconnected`);
    });
    


    socket.on('chat', data => {
        console.log(data);
        console.log(`${data.sender} has sent ${data.message} to ${data.roomName}`);
        socket.to(data.roomName).broadcast.emit("chat-message", data.message);
        // io.sockets.emit("chat-message", message, socket.id);
    });
});

console.log("socket server");

//dbConnection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/ChatDB', {
    useNewUrlParser: true,
    useUnifiedTopology: true

});

//body parser
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

routes(app);

app.get('/', (req, res) => {
    res.send('Node express is running on Port 4000');
});

app.get('/home', function(req, res){
    res.sendFile(__dirname + '/client/index.html');
  });

server.listen(PORT, '10.0.0.108');