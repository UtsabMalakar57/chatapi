import mongoose from 'mongoose'
import {
    LoginSchema,
    UserSchema,
    responseFormat
} from '../model/cemModel'

const Login = mongoose.model('Login', LoginSchema);
const User = mongoose.model('User', UserSchema);

export const loginUser = (req, res) => {
    User.findOne({ username: req.body.username }, (err, user) => {
        if (user == null || err) {
            res.send(`no user found.`)
        } else if (user.password == req.body.password) {
            let responseBody = responseFormat;
            responseBody.message = 'Successfully created';
            responseBody.body = user;
            res.json(responseBody);
        } else {
            throw err;
        }
    })
};

export const addNewUser = async (req, res) => {
    let newUser = new User(req.body);
    console.log(newUser.username);
    const docs = await User.aggregate([{ $match: { 'username': req.body.username } }]);
    console.log(docs)
    if(docs == null || docs.length == 0){
        newUser.save((err, new_user) => {
            if (err) {
                res.send(err);
            }
            let responseBody = responseFormat;
            responseBody.message = 'Successfully created';
            responseBody.body = new_user;
            res.json(responseBody);
        })
    }else{
        res.send('User already exist');
    }

};

