import mongoose from "mongoose"
import uuid from 'uuid'

const Schema = mongoose.Schema;

export const LoginSchema = new Schema({
    username:{
        type: String,
        required: 'Enter username'
    },
    password:{
        type: String,
        required:'Please enter a password'
    }
});

export const UserSchema = new Schema({
    userId : {
        type: String,
        default: uuid()
    },
    username:{
        type: String,
        required: 'Enter username'
    },
    password:{
        type: String,
        required:'Please enter a password'
    },
    firstName:{
        type: String,
    },
    lastName:{
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }

});

export const responseFormat = {
    status: "200",
    message: "",
    data: {}
};