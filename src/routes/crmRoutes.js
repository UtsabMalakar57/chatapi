import { 
    loginUser,
    addNewUser,
 } from "../controller/crmContoller";

 const route = (app) => {
     app.route('/login')
        .post((req,res,next) => {
            console.log(`Request from ${req.method} from ${req.originalUrl}`);
            next();
        }, loginUser);
    
    app.route('/create-user') 
        .post((req,res,next) => {
            console.log(`Request from ${req.method} from ${req.originalUrl}`);
            next();
        },addNewUser);

 };

 export default route;